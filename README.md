# **Drew O. Letcher**

_drewlab on gitlab_

I am a Sr. Data Engineer based in Iowa City, Iowa. Check out my profile and
job history on [Linked In](https://www.linkedin.com/in/drew-o-letcher)

[dictadata.org](https://www.dictadata.org) is an open source project for data management and analytic workflows specializing in Civic Data data wrangling and geo analytics.

[Music Theory Basics](https://www.drewletcher.net/musictheory) is a Vue.js app for referencing music scales, chords and progressions.
